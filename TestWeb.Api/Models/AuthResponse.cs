﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWeb.Api.Models
{
    [Serializable]
    public class AuthResponse
    {
        /// <summary>
        /// Succeeded
        /// </summary>
        public bool Succeeded { get; set; }

        /// <summary>
        /// RequiresTwoFactor
        /// </summary>
        public bool RequiresTwoFactor { get; set; }

        /// <summary>
        /// IsLockedOut
        /// </summary>
        public bool IsLockedOut { get; set; }

        /// <summary>
        /// HasUserDefaultPassword
        /// </summary>
        public bool HasUserDefaultPassword { get; set; }

        /// <summary>
        /// UserEmail
        /// </summary>
        public string UserEmail { get; set; }

        /// <summary>
        /// LockoutMessage
        /// </summary>
        public string LockoutMessage { get; set; }

        /// <summary>
        /// IsNonActive
        /// </summary>
        public bool IsNonActive { get; set; }

        /// <summary>
        /// StatusMessage
        /// </summary>
        public string StatusMessage { get; set; }

        /// <summary>
        /// IsPasswordExpired
        /// </summary>
        public bool IsPasswordExpired { get; set; }

        /// <summary>
        /// PasswordWillExpire
        /// </summary>
        public bool WillPasswordExpire { get; set; }

        /// <summary>
        /// PasswordExpireMessage
        /// </summary>
        public string PasswordExpireMessage { get; set; }

        /// <summary>
        /// ResetPasswordToken
        /// </summary>
        public string ResetPasswordToken { get; set; }
    }

    public class AuthRequest
    {
        public string UsernameOrEmail { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }
    }
}
