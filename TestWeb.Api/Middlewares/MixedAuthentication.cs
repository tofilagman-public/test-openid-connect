﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWeb.Api.Middlewares
{
    public static class MixedAuthentication
    {
        public static IApplicationBuilder UseMixedAuthentication(this IApplicationBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            return app.UseMiddleware<MixedAuthenticationMiddleware>();
        }
    }

    public class MixedAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// AzureAdAuthMiddleware constructor
        /// </summary>
        /// <param name="next"></param>
        /// <param name="schemes"></param>
        public MixedAuthenticationMiddleware(RequestDelegate next, IAuthenticationSchemeProvider schemes)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            Schemes = schemes ?? throw new ArgumentNullException(nameof(schemes));
        }

        /// <summary>
        /// Authentication schemes provider
        /// </summary>
        public IAuthenticationSchemeProvider Schemes { get; set; }

        /// <summary>
        /// Middleware invoke method
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            context.Features.Set<IAuthenticationFeature>(new AuthenticationFeature
            {
                OriginalPath = context.Request.Path,
                OriginalPathBase = context.Request.PathBase
            });

            // Give any IAuthenticationRequestHandler schemes a chance to handle the request
            var handlers = context.RequestServices.GetRequiredService<IAuthenticationHandlerProvider>();
            foreach (var scheme in await Schemes.GetRequestHandlerSchemesAsync())
            {
                var handler = await handlers.GetHandlerAsync(context, scheme.Name) as IAuthenticationRequestHandler;
                if (handler != null && await handler.HandleRequestAsync())
                {
                    return;
                }
            }

            var cookiesAuthenticate = await Schemes.GetSchemeAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var result = (cookiesAuthenticate != null)
                ? await context.AuthenticateAsync(CookieAuthenticationDefaults.AuthenticationScheme)
                : null;

            if (result?.Principal != null)
            {
                context.User = result.Principal;
            }
            else
            {
                var identityAuthenticate = await Schemes.GetSchemeAsync(IdentityConstants.ApplicationScheme);

                result = (identityAuthenticate != null)
                    ? await context.AuthenticateAsync(IdentityConstants.ApplicationScheme)
                    : null;

                if (result?.Principal != null)
                {
                    context.User = result.Principal;
                }
            }

            await _next(context);
        }
    }
}
