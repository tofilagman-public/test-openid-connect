﻿using AspNet.Security.OAuth.Validation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWeb.Api.Middlewares
{
    public static class OAuthValidation
    {

        public static IApplicationBuilder UseOAuthValidation(this IApplicationBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            return app.UseMiddleware<OAuthValidationMiddleware>();
        }

    }

    /// <summary>
    /// Middleware for the OAuth2 Bearer token validation
    /// </summary>
    public class OAuthValidationMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// OAuthValidationMiddleware constructor
        /// </summary>
        /// <param name="next"></param>
        /// <param name="schemes"></param>
        public OAuthValidationMiddleware(RequestDelegate next, IAuthenticationSchemeProvider schemes)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            Schemes = schemes ?? throw new ArgumentNullException(nameof(schemes));
        }

        /// <summary>
        /// Authentication schemes provider
        /// </summary>
        public IAuthenticationSchemeProvider Schemes { get; set; }

        /// <summary>
        /// Middleware invoke method
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            context.Features.Set<IAuthenticationFeature>(new AuthenticationFeature
            {
                OriginalPath = context.Request.Path,
                OriginalPathBase = context.Request.PathBase
            });

            // Give any IAuthenticationRequestHandler schemes a chance to handle the request
            var handlers = context.RequestServices.GetRequiredService<IAuthenticationHandlerProvider>();
            foreach (var scheme in await Schemes.GetRequestHandlerSchemesAsync())
            {
                var handler = await handlers.GetHandlerAsync(context, scheme.Name) as IAuthenticationRequestHandler;
                if (handler != null && await handler.HandleRequestAsync())
                {
                    return;
                }
            }

            var oAuthValidationAuthenticate = await Schemes.GetSchemeAsync(OAuthValidationDefaults.AuthenticationScheme);
            if (oAuthValidationAuthenticate != null)
            {
                var result = await context.AuthenticateAsync(oAuthValidationAuthenticate.Name);
                if (result?.Principal != null)
                {
                    context.User = result.Principal;
                }
            }

            await _next(context);
        }
    }
}
