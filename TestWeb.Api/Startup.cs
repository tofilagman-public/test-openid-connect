﻿using System;
using System.Reflection;
using AspNet.Security.OpenIdConnect.Primitives;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TestWeb.Api.Identity;
using TestWeb.Api.Options;
using TestWeb.Data;
using OpenIddict.Core;
using System.Threading;
using System.Threading.Tasks;
using OpenIddict.Models;
using TestWeb.Data.Entities;
using AutoMapper;
using TestWeb.Api.Middlewares;

namespace TestWeb.Api
{
    public class Startup
    { 
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(env.ContentRootPath)
              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
              .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
              .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            var connectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddCors();

            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(connectionString, sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly));
                options.UseOpenIddict();
            });

            IdentityBuilder identityBuilder = services.AddIdentityWithoutAuthentication(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;

                // User settings
                options.User.RequireUniqueEmail = true;
                options.SignIn.RequireConfirmedEmail = false;

                // Configure Identity to use the same JWT claims as OpenIddict
                options.ClaimsIdentity.UserNameClaimType = OpenIdConnectConstants.Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = OpenIdConnectConstants.Claims.Role;

            });
 
            var authBuilder = services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = IdentityConstants.ApplicationScheme;
                options.DefaultChallengeScheme = IdentityConstants.ApplicationScheme;
            })
              .AddCookie(IdentityConstants.ApplicationScheme, o =>
              {
                  o.LoginPath = new PathString("/Auth/Login");
                  o.Events = new CookieAuthenticationEvents
                  {
                      OnValidatePrincipal = SecurityStampValidator.ValidatePrincipalAsync
                  };
              });

            identityBuilder
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddUserStore<AppUserStore>()
                .AddDefaultTokenProviders();

            authBuilder.AddOAuthValidation();

            services.AddMvc(options =>
            {
                options.Filters.Add(new ProducesAttribute("application/json"));

                options.Filters.Add(new ResponseCacheAttribute
                {
                    NoStore = true
                });
            });

            services.Configure<ConnectionStringsOptions>(Configuration.GetSection("ConnectionStrings"));
            services.Configure<AppSettingOptions>(Configuration.GetSection("AppSettings"));

            //scopes

            services.AddOpenIddict()
                .AddEntityFrameworkCoreStores<ApplicationDbContext>()
                .AddMvcBinders()

                .EnableAuthorizationEndpoint("/connect/authorize")
                .EnableLogoutEndpoint("/connect/logout")
                //.EnableUserinfoEndpoint("/api/users/userinfo")

                //this function already set no need to define
                //this endpoint is the gateway from other system or sub system
                .EnableIntrospectionEndpoint("/connect/introspect") 

                .EnableTokenEndpoint("/connect/token")


                .AllowImplicitFlow()
                .AllowPasswordFlow()

                .SetAccessTokenLifetime(TimeSpan.FromMinutes(10))
                .DisableHttpsRequirement()
                .AddEphemeralSigningKey(); //for dev only

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        { 
            app.UseCors(builder =>
            {
                var corsOrigins = Configuration["AppSettings:CorsOrigins"].Split(',');

                builder.WithOrigins(corsOrigins);

                var corsHeader = Configuration["AppSettings:CorsHeader"];
                if (corsHeader.IsNotNullOrEmpty())
                {
                    builder.WithHeaders(corsHeader);
                }

                builder.AllowAnyMethod();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
             
            app.UseAuthentication(); //activate OAuth
             
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Auth}/{action=Index}/{id?}"
                    ); 
            });

            InitializeAsync(app.ApplicationServices, CancellationToken.None).GetAwaiter().GetResult(); 
        }


        private async Task InitializeAsync(IServiceProvider services, CancellationToken cancellationToken)
        {

            var clientId = Configuration["AppSettings:ClientId"];

            using (var scope = services.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var manager = scope.ServiceProvider.GetRequiredService<OpenIddictApplicationManager<OpenIddictApplication>>();

                // Validate public app
                var clientApp = await manager.FindByClientIdAsync(clientId, cancellationToken);

                if (clientApp == null)
                {
                    clientApp = new OpenIddictApplication
                    {
                        ClientId = clientId,
                        DisplayName = clientId
                    };

                    await manager.CreateAsync(clientApp, cancellationToken);

                    //this should be public type hence client app will throw Response_Type error
                }

                var logoutRedirectUri = Configuration["AppSettings:LogoutRedirectUri"];
                var redirectUri = Configuration["AppSettings:LoginRedirectUri"];

                if (logoutRedirectUri.IsNotNullOrEmpty() && logoutRedirectUri != clientApp.PostLogoutRedirectUris || redirectUri.IsNotNullOrEmpty() && redirectUri != clientApp.RedirectUris)
                {

                    clientApp.PostLogoutRedirectUris = logoutRedirectUri;
                    clientApp.RedirectUris = redirectUri;

                    await manager.UpdateAsync(clientApp, cancellationToken);
                }

                //confi client
                var clientConfi = Configuration["AppSettings:ConfiClientId"];

                if (await manager.FindByClientIdAsync(clientConfi, cancellationToken) == null)
                {
                    var appConfi = new OpenIddictApplication { ClientId = clientConfi };

                    //Note: Client secret cannot be associated with public application, so we need to create another client id with secret
                    await manager.CreateAsync(appConfi, Configuration["AppSettings:ClientSecret"], cancellationToken);
                }

                //add default user

                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
                var appUser = Configuration["AppSettings:SysUser"];


                if (await userManager.FindByIdAsync(appUser) == null)
                {
                    var email = $"{appUser}@somemail.net";
                    var newUser = new AppUser
                    {
                        EmailConfirmed = true,
                        Email = email,
                        LockoutEnabled = true,
                        UserName = appUser
                    };

                    await userManager.CreateAsync(newUser, Configuration["AppSettings:DefaultPassword"]);
                }

            }
        }
    }
}
