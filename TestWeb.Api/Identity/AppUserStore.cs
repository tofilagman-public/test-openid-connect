﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TestWeb.Data;
using TestWeb.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace TestWeb.Api.Identity
{
    public class AppUserStore : UserStore<AppUser>
    {

        public AppUserStore(ApplicationDbContext context, IdentityErrorDescriber describer)
           : base(context, describer) { }

        public override Task<AppUser> FindByIdAsync(string userId, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            //var id = ConvertIdFromString(userId);

            //return Users.Include(x => x.Claims).SingleOrDefaultAsync(x => x.UserName == id);

            return base.FindByIdAsync(userId, cancellationToken);
        }
    }
}
