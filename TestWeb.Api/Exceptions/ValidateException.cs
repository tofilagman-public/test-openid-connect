﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWeb.Api.Exceptions
{
    /// <summary>
    /// ValidateException
    /// </summary>
    public class ValidateException : Exception
    {
        /// <summary>
        /// Name of property that contains invalid value.
        /// </summary>
        public string PropertyName { get; private set; }

        /// <summary>
        /// Reason value is invalid.
        /// </summary>
        public string ValidationMessage { get; private set; }

        /// <summary>
        /// Constructor using a property name, and a validation error message
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="message"></param>
        public ValidateException(string propertyName, string message)
        {
            PropertyName = propertyName ?? string.Empty;
            ValidationMessage = message;
        }

        /// <summary>
        /// Constructor using a property name, and a validation error message
        /// as well as the underlying inner exception
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public ValidateException(string propertyName, string message, Exception innerException)
            : base(message, innerException)
        {
            PropertyName = propertyName ?? string.Empty;
            ValidationMessage = message;
        }
    }
}
