﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestWeb.Api.Exceptions;
using TestWeb.Api.Models;
using TestWeb.Data.Entities;

namespace TestWeb.Api.Controllers
{
    [Authorize]
    public class AuthController : Controller
    {

        private readonly UserManager<AppUser> UserManager;
        private readonly SignInManager<AppUser> SignInManager;

        public AuthController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            this.UserManager = userManager;
            this.SignInManager = signInManager;
        }

        [HttpGet, AllowAnonymous]
        public IActionResult Index()
        {
            return RedirectToAction(nameof(Login));
        }

        [HttpGet, AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewData["Title"] = "Test Login";
            return View();
        }

        [HttpPost, AllowAnonymous]
        public async Task<AuthResponse> Login(AuthRequest authRequest)//[FromBody]
        {
            var user = await Validate(authRequest);
            var authResp = new AuthResponse();

            if (user != null)
            {
                var validPassword = await UserManager.CheckPasswordAsync(user, authRequest.Password);

                if (validPassword)
                {
                    var res = await SignInManager.PasswordSignInAsync(user, authRequest.Password, false, false);

                    authResp.Succeeded = res.Succeeded;

                }
            }

            return authResp;
        }

        private async Task<AppUser> Validate(AuthRequest authRequest)
        {
            if (string.IsNullOrWhiteSpace(authRequest.UsernameOrEmail))
                throw new ValidateException("UsernameOrEmail", "Please enter your user name or e-mail");

            if (string.IsNullOrWhiteSpace(authRequest.Password))
                throw new ValidateException("Password", "Please enter your password");

            // Require the user to have a confirmed email before they can log on.
            var user = await UserManager.FindByNameAsync(authRequest.UsernameOrEmail);
            if (user == null)
                user = await UserManager.FindByEmailAsync(authRequest.UsernameOrEmail);

            if (user != null)
            {
                if (!await UserManager.IsEmailConfirmedAsync(user))
                {
                    throw new ValidateException("UsernameOrEmail", "You must have a confirmed email to log in.");
                }
            }

            return user;
        }
    }
}
