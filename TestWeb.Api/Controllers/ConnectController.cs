﻿using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;
using AspNet.Security.OpenIdConnect.Server;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OpenIddict.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using TestWeb.Api.Options;
using TestWeb.Data.Entities;

namespace TestWeb.Api.Controllers
{
    public class ConnectController : Controller
    {
        private readonly SignInManager<AppUser> SignInManager;
        private readonly UserManager<AppUser> UserManager;
        private readonly AppSettingOptions AppSettings;
        private readonly IHostingEnvironment Environment;

        public ConnectController(SignInManager<AppUser> signInManager, UserManager<AppUser> userManager, IOptionsMonitor<AppSettingOptions> appSettingsOptions, IHostingEnvironment env)
        {
            this.SignInManager = signInManager;
            this.UserManager = userManager;
            this.AppSettings = appSettingsOptions.CurrentValue;
            this.Environment = env;
        }

        [Authorize, HttpGet] //Authorize,
        public async Task<IActionResult> Authorize(OpenIdConnectRequest req)
        {
            AppUser user = null;
            var originalClaims = new List<Claim>();

            user = await UserManager.GetUserAsync(User);

            if (user == null)
            {
                return Redirect(nameof(Logout));
            }

            // Create a new authentication ticket.
            var ticket = await CreateTicketAsync(req, user, originalClaims);

            // Returning a SignInResult will ask OpenIddict to issue the appropriate access/identity tokens.
            return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        { 
            // Ask ASP.NET Core Identity to delete the local and external cookies created
            // when the user agent is redirected from the external identity provider
            // after a successful authentication flow (e.g Google or Facebook).
            await SignInManager.Context.SignOutAsync(IdentityConstants.ApplicationScheme);

            foreach (var cookie in Request.Cookies.Keys)
            {
                if (!cookie.Contains(".AspNetCore.Identity.Application"))
                {
                    Response.Cookies.Delete(cookie);
                }
            }

            return SignOut();
        }

        /// <summary>
        /// Endpoint for Client Credentials Flow (?)
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Produces("application/json")]
        public async Task<IActionResult> Token(OpenIdConnectRequest req)
        {
            Debug.Assert(req.IsTokenRequest(),
                "The OpenIddict binder for ASP.NET Core MVC is not registered. " +
                "Make sure services.AddOpenIddict().AddMvcBinders() is correctly called.");

            if (req.IsPasswordGrantType())
            {
                var user = await UserManager.FindByNameAsync(req.Username);
                if (user == null)
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.InvalidGrant,
                        ErrorDescription = "The username/password couple is invalid."
                    });
                }

                // Validate the username/password parameters and ensure the account is not locked out.
                var result = await SignInManager.CheckPasswordSignInAsync(user, req.Password, lockoutOnFailure: true);
                if (!result.Succeeded)
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.InvalidGrant,
                        ErrorDescription = "The username/password couple is invalid."
                    });
                }

                // Create a new authentication ticket.
                var ticket = await CreateTicketAsync(req, user);

                return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
            }

            return BadRequest(new OpenIdConnectResponse
            {
                Error = OpenIdConnectConstants.Errors.UnsupportedGrantType,
                ErrorDescription = "The specified grant type is not supported."
            });
        }

        private async Task<AuthenticationTicket> CreateTicketAsync(OpenIdConnectRequest request, AppUser user, IEnumerable<Claim> originalClaims = null)
        {
            // Create a new ClaimsPrincipal containing the claims that
            // will be used to create an id_token, a token or a code.
            var principal = await SignInManager.CreateUserPrincipalAsync(user);

            foreach (var claim in principal.Claims)
            {
                // In this sample, every claim is serialized in both the access and the identity tokens.
                // In a real world application, you'd probably want to exclude confidential claims
                // or apply a claims policy based on the scopes requested by the client application.
                claim.SetDestinations(
                    OpenIdConnectConstants.Destinations.AccessToken,
                    OpenIdConnectConstants.Destinations.IdentityToken);
            }

            var userIdClaim = new Claim("userId", user.Id);
            userIdClaim.SetDestinations(OpenIdConnectConstants.Destinations.AccessToken);

            var authenticationTypeClaim = new Claim("authenticationType", "test"); //_isohSettingsOptions.Authentication.Type
            authenticationTypeClaim.SetDestinations(OpenIdConnectConstants.Destinations.IdentityToken);

            var identity = (ClaimsIdentity)principal.Identity;
            identity.AddClaim(userIdClaim);
            identity.AddClaim(authenticationTypeClaim);

            if (originalClaims != null)
            {
                foreach (var claim in originalClaims)
                {
                    claim.SetDestinations(OpenIdConnectConstants.Destinations.IdentityToken);
                    identity.AddClaim(claim);
                }
            }

            // Create a new authentication ticket holding the user identity.
            var ticket = new AuthenticationTicket(
                principal,
                new AuthenticationProperties(),
                OpenIdConnectServerDefaults.AuthenticationScheme);

            // Set the list of scopes granted to the client application.
            ticket.SetScopes(
                new[]
                {
                    OpenIdConnectConstants.Scopes.OpenId,
                    OpenIdConnectConstants.Scopes.Email,
                    OpenIdConnectConstants.Scopes.Profile,
                    OpenIddictConstants.Scopes.Roles
                }.Intersect(request.GetScopes()));

            ticket.SetResources(AppSettings.ConfiClientId); //this will switch to another client which isolates the request ref: Startup:210

            return ticket;
        }

        private IActionResult SignOut()
        {
            var redirect = "/Auth/Login?reason=session&return=";
            if (!Environment.IsDevelopment())
            {
                redirect = "/api" + redirect;
            }

            // if the referer has auto-logout in the path
            //  - stop "normal" flow
            //  - redirect to login with appended querystring to signal
            //    the redirect was via inactivity or session expiration
            var pathAndQuery = new Uri(Request.Headers["Referer"]).PathAndQuery;
            if (pathAndQuery.Contains("auto-logout"))
            {
                return Redirect(redirect + WebUtility.UrlEncode(AppSettings.LogoutRedirectUri));
            }

            // else use "normal" flow
            // Returning a SignOutResult will ask OpenIddict to redirect the user agent
            // to the post_logout_redirect_uri specified by the client application.
            return SignOut(OpenIdConnectServerDefaults.AuthenticationScheme);
        }


    }
}
