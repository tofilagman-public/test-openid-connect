﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWeb.Api.Options
{
    public class ConnectionStringsOptions : IConnectionStringsOptions
    {
        // To enable Ssl add 'Encrypt=true; TrustServerCertificate=true;' to the connection string in appsettings.json
        public string DefaultConnection { get; set; }
    }

    public interface IConnectionStringsOptions
    {
        string DefaultConnection { get; }
    }
}
