﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWeb.Api.Options
{
    public class AppSettingOptions  
    {
        public string ClientId { get; set; }
        public string ConfiClientId { get; set; }
        public string ClientSecret { get; set; }
        public string LogoutRedirectUri { get; set; }
        public string LoginRedirectUri { get; set; } 
        public string SysUser { get; set; }
        public string DefaultPassword { get; set; }
    }

}
