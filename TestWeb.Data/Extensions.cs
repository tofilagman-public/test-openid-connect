﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestWeb.Data
{
    public static class Extensions
    {

        public static bool IsNotNullOrEmpty(this string data) => !string.IsNullOrEmpty(data);

    }
}
