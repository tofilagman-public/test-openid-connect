# Simple OpenID Authentication using .NetCore and Angular
 
Requirements:
  - Visual Studio >= 2017
  - NodeJs v10^
  - SQL Server 2012^

Setup:
 - Database
 -- Create a new Database Named: **TestDB**
 - Auth Server
 -- Navigate to TestWeb.Api Project
 -- Open **appsettings.json**
 -- Change the connection string according to your database setup
    ```
    "ConnectionStrings": {
    "DefaultConnection": "Server=localhost\\SQL2014;Database=TestDB;Trusted_Connection=True;MultipleActiveResultSets=true",
    "Comment": "To enable Ssl add 'Encrypt=true; TrustServerCertificate=true;' to the connection string"
    },
    ```
    -- On Package Manager Console (PMC) - this will restore all online dependencies
    ```
    nuget restore
    ```
    -- Next, we have to migrate some open id tables
    -- Navigate to folder **/Migrations**
    -- On PMC
    ```
    Update-Database
    ```
    -- Note: this will execute the migration setup from **20190329020041_OpenIdRc1.cs** object
    -- for better docs: https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/
- Client Application
-- Navigate to client-angular project
-- on *cmd* 
    ```
    npm install
    ```
    -- next you have to install angular cli helper
    ```
    npm install -g @angular/cli
    ```
    -- Build
    ```
    ng build 
    ```

Running the Application:

- On VS Click Debug or Run
- this will run under port : 58100 which will open the default login page
- Next: we have to run the client app that call the Auth Login interface
- Navigate to **client-angular** project
- On Terminal or CMD
    ```
    ng serve
    ```
- Now it will run under port 4200
- Open your browser and navigate to http://localhost:4200
 
**Note:**
1. you will see the transition between authentication server and the client app.

