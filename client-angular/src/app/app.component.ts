import { AuthService } from './services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  profile: any;

  constructor(private authService: AuthService) {

  }

  ngOnInit() {
    if (this.authService.isLoggedIn())
      this.profile = this.authService.getClaims();
  }

  SignOut() {
    this.authService.endAuthentication();
    //   // .then(()=> {
    //   //   window.localStorage.clear();
    // });
  }

}
