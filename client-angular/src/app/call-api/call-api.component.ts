import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-call-api',
  templateUrl: './call-api.component.html',
  styleUrls: ['./call-api.component.scss']
})
export class CallApiComponent implements OnInit {
  response: Object;

  constructor(private http: HttpClient, private authservice: AuthService) { }

  ngOnInit() {
    let headerss = new HttpHeaders({ 'Authorization': this.authservice.getAuthorizationHeaderValue()});

    this.http.get('http://localhost:58100/api', {headers: headerss})
    .subscribe(rep=> this.response = rep);
  }

}
